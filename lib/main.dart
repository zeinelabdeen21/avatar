import 'package:flutter/material.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

import 'app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // LIST_OF_LANGS = ['ar', 'en'];
  // LANGS_DIR = 'assets/langs/';
  await translator.init(
    languagesList: ['ar', 'en'],
    assetsDirectory: 'assets/langs/',
    localeDefault: LocalizationDefaultType.asDefined,
  );
  runApp(
    LocalizedApp(
      child: MyApp(),
    ),
  );
}
