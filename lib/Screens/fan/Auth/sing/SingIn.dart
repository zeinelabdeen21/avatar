import 'package:avatar_master/Screens/LoginType/LoginTypeView.dart';
import 'package:avatar_master/Screens/fan/Auth/register/singup.dart';
import 'package:avatar_master/Screens/fan/Home/fanHome.dart';
import 'package:avatar_master/helpers/AlreadyHaveAccount.dart';
import 'package:avatar_master/helpers/Btns.dart';
import 'package:avatar_master/helpers/Loaders.dart';
import 'package:avatar_master/helpers/TextFormFields.dart';
import 'package:avatar_master/helpers/app_theme.dart';
import 'package:avatar_master/helpers/navotton.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:localize_and_translate/localize_and_translate.dart';

class FanSingIn extends StatefulWidget {
  @override
  _FanSingInState createState() => _FanSingInState();
}

class _FanSingInState extends State<FanSingIn> {
  bool _loading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            translator.currentLanguage == "en"
                ? Icons.keyboard_arrow_left_rounded
                : Icons.arrow_right_alt,
            color: Color(0xFF3f3f3f),
            size: 25,
          ),
          onPressed: () {
            Get.to(
              LoginTypeView(),
            );
          },
        ),
      ),
      body: Form(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          // padding: EdgeInsets.only(
          //   left: 30,
          //   right: 30,
          // ),
          child: SingleChildScrollView(
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(
                        right: 15,
                        left: 15,
                      ),
                      child: Text(
                        translator.currentLanguage == "en"
                            ? "Log in as a fan"
                            : "الدخول كامشجع",
                        style: TextStyle(
                          fontSize: 34,
                          fontWeight: FontWeight.w900,
                          color: Color(0xFF535461),
                        ),
                      ),
                    ),
                    Container(
                      padding: translator.currentLanguage == "en"
                          ? EdgeInsets.only(
                              top: 30,
                              left: 15,
                            )
                          : EdgeInsets.only(
                              top: 30,
                              right: 15,
                            ),
                      width: MediaQuery.of(context).size.width,
                      child: Stack(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image(
                            image: AssetImage(
                              "assets/icons/icon-login-1.png",
                            ),
                            width: 83,
                            // height: 62,
                            fit: BoxFit.cover,
                          ),
                          translator.currentLanguage == "en"
                              ? Positioned(
                                  right: 0,
                                  child: Image(
                                    image: AssetImage(
                                      "assets/icons/canvas1.png",
                                    ),
                                    // width: 83,
                                    // height: 62,
                                    fit: BoxFit.cover,
                                  ),
                                )
                              : Positioned(
                                  left: 0,
                                  child: Image(
                                    image: AssetImage(
                                      "assets/icons/canvas1.png",
                                    ),
                                    // width: 83,
                                    // height: 62,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: translator.currentLanguage == "en"
                      ? EdgeInsets.only(
                          top: 30,
                        )
                      : EdgeInsets.only(
                          top: 30,
                        ),
                  child: Column(
                    children: [
                      txtField(
                        context: context,
                        controller: null,
                        enabled: true,
                        hintText: translator.currentLanguage == "en"
                            ? "mobile"
                            : "ارقم الجوال",
                        textInputType: TextInputType.phone,
                        validator: (String val) {
                          if (val.isEmpty)
                            return translator.currentLanguage == "en"
                                ? "Mobile number is required"
                                : "رقم الجوال مطلوب";
                          else
                            return null;
                        },
                        onSaved: (String val) {
                          setState(() {});
                        },
                        obscureText: false,
                        // prefix: "assets/icons/phone.png",
                      ),
                      SizedBox(
                        height: AppTheme.sizedBoxHeight,
                      ),
                      txtField(
                        context: context,
                        validator: (String val) {
                          if (val.isEmpty)
                            return translator.currentLanguage == "en"
                                ? " Password is required"
                                : "كلمة المرور مطلوبة";
                          else
                            return null;
                        },
                        onSaved: (String val) {
                          setState(() {});
                        },
                        controller: null,
                        textInputType: TextInputType.visiblePassword,
                        enabled: true,
                        obscureText: obscureText,
                        hintText: translator.currentLanguage == "en"
                            ? "Password"
                            : "كلمة المرور",
                      ),
                      SizedBox(
                        height: AppTheme.sizedBoxHeight,
                      ),
                      forgetPasswordText(context, () {
                        // Get.to(
                        //   // ForgetPasswordView(
                        //   //   type: "client",
                        //   // ),
                        // );
                      }),
                      _loading
                          ? authLoader()
                          : btn(
                              context,
                              translator.currentLanguage == "en"
                                  ? "Sign in"
                                  : "تسجيل الدخوال", () {
                              Get.to(
                                FanHomeView(),
                              );
                            }),
                      btnB(
                          context,
                          translator.currentLanguage == "en"
                              ? "Sign in"
                              : "تسجيل حساب جديد", () {
                        Get.to(
                          FanRegister(),
                        );
                      }),
                    ],
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Navotton(
                  context,
                ),
                // Positioned(
                //   bottom: 0,
                //   child: Container(
                //     width: MediaQuery.of(context).size.width,
                //     // height: MediaQuery.of(context).size.height,
                //     padding: EdgeInsets.only(
                //       top: 15,
                //       bottom: 15,
                //     ),
                //     alignment: Alignment.topCenter,
                //     decoration: BoxDecoration(
                //       color: Colors.white,
                //       borderRadius: BorderRadius.only(
                //         topRight: Radius.circular(30.0),
                //         topLeft: Radius.circular(30.0),
                //       ),
                //       boxShadow: [
                //         BoxShadow(
                //           color: Colors.black38,
                //           spreadRadius: 10,
                //           blurRadius: 30,
                //           offset: Offset(0, 30), // changes position of shadow
                //         ),
                //       ],
                //     ),
                //     child: Column(
                //       children: [
                //         Padding(
                //           padding: const EdgeInsets.only(
                //             top: 0,
                //           ),
                //           child: Text(
                //             "أو عن طريق",
                //             style: TextStyle(
                //               fontSize: 22,
                //               fontWeight: FontWeight.bold,
                //               color: Color(0xFF535461),
                //             ),
                //           ),
                //         ),
                //         Padding(
                //           padding: const EdgeInsets.only(
                //             top: 0,
                //           ),
                //           child: Row(
                //             mainAxisAlignment: MainAxisAlignment.center,
                //             children: [
                //               IconButton(
                //                 icon: FaIcon(FontAwesomeIcons.twitterSquare),
                //                 color: Color(0xFF30c1ef),
                //                 iconSize: 30,
                //                 onPressed: () => launch(
                //                     'https://github.com/himanshusharma89'),
                //               ),
                //               IconButton(
                //                 icon: FaIcon(FontAwesomeIcons.googlePlusG),
                //                 color: Color(0xFFd93f21),
                //                 iconSize: 30,
                //                 onPressed: () => launch(
                //                     'https://github.com/himanshusharma89'),
                //               ),
                //               IconButton(
                //                 icon: FaIcon(FontAwesomeIcons.facebookSquare),
                //                 color: Color(0xFF1976d2),
                //                 iconSize: 30,
                //                 onPressed: () => launch(
                //                     'https://github.com/himanshusharma89'),
                //               ),
                //             ],
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool obscureText = true;
}
