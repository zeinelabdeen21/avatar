import 'package:avatar_master/Screens/fan/Home/fanHome.dart';
import 'package:avatar_master/Screens/fan/wdget/customListTile.dart';
import 'package:avatar_master/helpers/user.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class DrawerMenu extends StatelessWidget {
  const DrawerMenu({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserImg(),
          CustomListTile(
            onTap: () {
              Get.to(
                FanHomeView(),
              );
            },
            tit: "الرئيسيه",
            icons: FontAwesomeIcons.home,
          ),
          CustomListTile(
            onTap: () {},
            tit: "العضوية",
            icons: FontAwesomeIcons.solidIdBadge,
          ),
          CustomListTile(
            onTap: () {},
            tit: "العضوية",
            icons: FontAwesomeIcons.film,
          ),
          CustomListTile(
            onTap: () {},
            tit: "الرعاة",
            icons: FontAwesomeIcons.volumeUp,
          ),
          CustomListTile(
            onTap: () {},
            tit: "عن النادي",
            icons: FontAwesomeIcons.hotel,
          ),
          CustomListTile(
            onTap: () {},
            tit: "عن التطبيق",
            icons: FontAwesomeIcons.exclamationCircle,
          ),
          CustomListTile(
            onTap: () {},
            tit: "تواصل معنا",
            icons: FontAwesomeIcons.envelopeOpen,
          ),
          CustomListTile(
            onTap: () {},
            tit: "تسجيل الخروج",
            icons: FontAwesomeIcons.signOutAlt,
          ),
          SizedBox(
            height: 60,
          ),
        ],
      ),
    );
  }
}
