import 'package:avatar_master/Screens/fan/Home/HomeNews/homeNews.dart';
import 'package:flutter/material.dart';

class AppBarScrein extends StatelessWidget {
  const AppBarScrein({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      toolbarHeight: 50,
      title: Image.asset(
        "assets/icons/icon-home.png",
        width: 30,
        height: 30,
        fit: BoxFit.cover,
      ),
      centerTitle: true,
      actions: [
        IconButton(
            icon: Icon(Icons.search),
            color: Color(0xFF6e262c),
            iconSize: 20,
            onPressed: () {
              showSearch(
                context: context,
                delegate: DataSearsh(),
              );
            })
      ],
      iconTheme: IconThemeData(
        color: Color(0xFF6e262c),
        size: 20,
      ),
      // leading: IconButton(
      //   icon: Icon(Icons.accessible),
      //   color: Colors.black,
      //   onPressed: () => Scaffold.of(context).openDrawer(),
      // ),
    );
  }
}
